# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 14:52:52 2022

@author: mateus.silva
"""

import pandas as pd
import numpy as np
import os
from .yaml_parser import yaml_parser
import idea_types.variables as var


class DataTreatment(object):
    def __init__(self, variables, lasers_list, laser_type):
        self.variables = variables
        self.lasers_list = lasers_list
        self.laser_type = laser_type
        
    def __save_xlsx__(self, sn_list, values_p, values_n, variable, ch, gb):
        
        frequency_dataframe = pd.DataFrame()
        
        writer1 = pd.ExcelWriter(os.path.join('data', self.laser_type.lower(), f'{variable}_{ch}_{gb}.xlsx'), engine='xlsxwriter')
                
        frequency_dataframe['Laser SN'] = sn_list
        
        frequency_dataframe['positive'] = values_p
        frequency_dataframe['negative'] = values_n
        
        config_parser = yaml_parser(config_path=r'criteria.yaml')
        config_dict = config_parser.open_yaml()
        
        limits_dict = config_dict['criteria'][variable]
        limits_dataframe = pd.DataFrame(limits_dict.items(), columns=[0, 1])
        
        limits_dataframe.to_excel(writer1, sheet_name='Sheet1', index = False, header = False)
        
        frequency_dataframe.to_excel(writer1, sheet_name='Sheet1', index=False, startrow=2, startcol=0)
        writer1.save()
        
    def __clean_data__(self, pickle, variable, ch, gb):
        
        freq_list_positive = []
        freq_list_negative = []
        
        freq_pos = pickle[(pickle['channel'] == ch) & (pickle['valid_temp'] > 0)][variable].values
        freq_neg = pickle[(pickle['channel'] == ch) & (pickle['valid_temp'] < 0)][variable].values
        tgt_freq = np.linspace(191250, 196100, 98)
        
        if gb == 'Master':
            tgt_power = 16.0
        else:
            tgt_power = 13.0
        
        #Treating values returned as []
        if freq_pos.size == 0:
            freq_pos = 0
        if freq_neg.size == 0:
            freq_neg = 0
            
        #Treating values returned as [list([x y z])]
        try:
            if freq_neg != 0 and (variable == var.FREQ_WM or variable == var.PM_PWR_dBm or variable == var.PD_POWER_dBm):
                if len(freq_neg[0]) == 0: #Prevents break from [list([ ])]
                    freq_neg = 0
                else:
                    freq_neg = freq_neg[0][-1]
        except:
            pass
        try:
            if freq_pos != 0 and (variable == var.FREQ_WM or variable == var.PM_PWR_dBm or variable == var.PD_POWER_dBm):
                if len(freq_pos[0]) == 0: #Prevents break from [list([ ])]
                    freq_pos = 0
                else:
                    freq_pos = freq_pos[0][-1]
        except:
            pass
        
        #Treating values [list([x y z]) list([x y z]) list([x y z])]
        try:
            if len(freq_pos[0]) > 1:
                freq_pos = freq_pos[0][-1]
        except:
            pass
        try:
            if len(freq_neg[0]) > 1:
                freq_neg = freq_neg[0][-1]
        except:
            pass
        
        #Treating values returned as [x y z]
        try:
            if len(freq_neg) >= 1 and (variable == 'tuning_time' or variable == 'total_consumption'):
                freq_neg = freq_neg[-1]
        except:
            pass
        try:
            if len(freq_pos) >= 1 and (variable == 'tuning_time' or variable == 'total_consumption'):
                freq_pos = freq_pos[-1]
        except:
            pass
        
        #Calculating deviations
        try:
            if variable == var.FREQ_WM:
                freq_list_positive.append(freq_pos-tgt_freq[ch-1])
                freq_list_negative.append(freq_neg-tgt_freq[ch-1])
            elif variable == var.PM_PWR_dBm or variable == var.PD_POWER_dBm:
                freq_list_positive.append(freq_pos-tgt_power)
                freq_list_negative.append(freq_neg-tgt_power)
            else:
                freq_list_positive.append(freq_pos)
                freq_list_negative.append(freq_neg)
        except:
            #print(f'freq_pos: {freq_pos} / freq_neg: {freq_neg} / SN: {dataframe.iloc[index]["Laser"]}\n')
            freq_list_positive.append(freq_pos)
            freq_list_negative.append(freq_neg)
        
        return (freq_list_positive.pop(), freq_list_negative.pop())
        
    def valid_info_dual(self, save_xlsx = True):
        dataframe = pd.DataFrame(self.lasers_list)
                
        values_positive = []
        values_negative = []
        
        SN_list = []
        
        for gb in ['Master', 'Slave']:
            for variable in self.variables:
                for ch in [1, 49, 98]:
                    for index in dataframe.index:         
                        try:
                            pickle = np.load(dataframe.iloc[index]['Pickle File'][0] if gb in dataframe.iloc[index]['Pickle File'][0] else dataframe.iloc[index]['Pickle File'][1], allow_pickle=True)
                            SN_list.append(dataframe.iloc[index]['Laser'])
                        except:
                            continue
                        
                        pickle.reset_index(inplace = True)
                                
                        value_positive, value_negative = self.__clean_data__(pickle, variable, ch, gb)
                        values_positive.append(value_positive)
                        values_negative.append(value_negative)
                        
                    if save_xlsx:
                        self.__save_xlsx__(SN_list, values_positive, values_negative, variable, ch, gb)
                        
                    values_negative = []
                    values_positive = []
                    SN_list = []
                    
    def valid_info_single(self, save_xlsx = True):
        dataframe = pd.DataFrame(self.lasers_list)
                
        values_positive = []
        values_negative = []
        
        SN_list = []
        
        for gb in ['Master']:
            for variable in self.variables:
                for ch in [1, 49, 98]:
                    for index in dataframe.index:         
                        try:
                            pickle = np.load(dataframe.iloc[index]['Pickle File'], allow_pickle=True)
                            SN_list.append(dataframe.iloc[index]['Laser'])
                        except:
                            continue
                        
                        pickle.reset_index(inplace = True)
                                
                        value_positive, value_negative = self.__clean_data__(pickle, variable, ch, gb)
                        values_positive.append(value_positive)
                        values_negative.append(value_negative)
                                
                    if save_xlsx:
                        self.__save_xlsx__(SN_list, values_positive, values_negative, variable, ch, gb)
                        
                    values_negative = []
                    values_positive = []
                    SN_list = []