# -*- coding: utf-8 -*-
"""
Created on Fri Mar 11 13:47:46 2022

@author: mateus.silva
"""

from idea_types import variables as var
import numpy as np
import glob
import os
import matplotlib.pyplot as plt
from datetime import datetime as dt
import warnings


class ScrapLaserParams(object):
    def __init__(self, main_path):
        self.main_path = main_path
        
    def __only_folders__(self, list_folders):
        folders = [os.path.join(self.main_path, f) for f in list_folders if os.path.isdir(os.path.join(self.main_path, f))]
        return folders
        
    def pickle_finder(self, val_folder, valid_dict):
        pkl_dual = []
        if len(val_folder) == 1:
            pkl_files = glob.glob(os.path.join(val_folder[0], '*', 'df_ok.pkl'), recursive=True)
            if len(pkl_files) > 1:
                newest_pkl = sorted(pkl_files, key = os.path.getctime, reverse = True)
                valid_dict['Pickle File'].append(newest_pkl[0])
            elif len(pkl_files) == 0:
                valid_dict['Pickle File'].append('')
            else:
                valid_dict['Pickle File'].append(pkl_files.pop())
        elif len(val_folder) > 1:
            for idx in val_folder:
                pkl_files = glob.glob(os.path.join(idx, '*', 'df_ok.pkl'), recursive=True)
                if len(pkl_files) > 1:
                    newest_pkl = sorted(pkl_files, key = os.path.getctime, reverse = True)
                    pkl_dual.append(newest_pkl[0])
                elif len(pkl_files) == 0:
                    pkl_dual.append('')
                else:
                    pkl_dual.append(pkl_files.pop())
            valid_dict['Pickle File'].append(pkl_dual)
            
    def list_lasers(self, num = 20, by = 'quantity'):  
        valid_lasers = {'Laser': list(), 'GB': list(),'Validation Path': list(), 'Pickle File': list()}
        
        lasers_sn = self.__only_folders__(os.listdir(self.main_path))
        lasers_sn = sorted(lasers_sn, key = os.path.getctime, reverse = True)[:num]
        
        for sn in lasers_sn:
            try:
                val_folders = glob.glob(os.path.join(sn, '*', 'Validation'), recursive = True)
                if(len(val_folders) > 0):
                    
                    board_number = sn.split('\\')[-1]
                    
                    if 'Single' in board_number:
                        gb_number = val_folders[0].split('\\')[-2]
                        valid_lasers['GB'].append(gb_number)
                        print(f'Validation test found for LASER: {board_number} and GB: {gb_number}.\nFolders created on {dt.fromtimestamp(os.path.getctime(val_folders[0]))}\n')
                        print('-------------------------------------------------------------------\n')
                    elif 'Dual' in board_number:
                        gb_number_0 = val_folders[0].split('\\')[-2]
                        gb_number_1 = val_folders[1].split('\\')[-2]
                        valid_lasers['GB'].append([gb_number_0, gb_number_1])
                        print(f'Validation test found for LASER: {board_number} and GB: {gb_number_0}.\nFolder created on {dt.fromtimestamp(os.path.getctime(val_folders[0]))}')
                        print(f'Validation test found for LASER: {board_number} and GB: {gb_number_1}.\nFolder created on {dt.fromtimestamp(os.path.getctime(val_folders[1]))}\n')
                        print('-------------------------------------------------------------------\n')
                    elif 'QSFP' in board_number:
                        gb_number = val_folders[0].split('\\')[-2]
                        valid_lasers['GB'].append(gb_number)
                        print(f'Validation test found for LASER: {board_number}. Folders created on {dt.fromtimestamp(os.path.getctime(val_folders[0]))}\n')
                        print('-------------------------------------------------------------------\n')
    
                    valid_lasers['Laser'].append(board_number)
                    valid_lasers['Validation Path'].append(val_folders)
                    self.pickle_finder(val_folders, valid_lasers)
            except: 
                continue
        if len(valid_lasers['Laser']) < num:
            warnings.warn('Number of valid lasers is below the threshold defined in function. There are laser units with Validation test undone')
        
        return valid_lasers
    
        
    

