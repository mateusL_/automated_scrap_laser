# -*- coding: utf-8 -*-
"""
Created on Fri Mar 11 13:48:33 2022

@author: mateus.silva
"""

import pandas as pd
import idea_types.variables as var
from common.scrap_class import ScrapLaserParams
from common.data_treatment_class import DataTreatment

scrap_object = ScrapLaserParams(main_path=r'G:\.shortcut-targets-by-id\1hEjfj6xr_TL7LBHky4q8hkBH9vULMO6w\Nano-ITLA\Test and Calibration')
laser_list = scrap_object.list_lasers(num=300)

dataframe = pd.DataFrame(laser_list)
frequency_dataframe_negative = pd.DataFrame()
frequency_dataframe_positive = pd.DataFrame()

variables = [var.FREQ_WM, var.PM_PWR_dBm, 'total_consumption', 'tuning_time']

data_dual = DataTreatment(variables, laser_list, 'Dual')
data_dual.valid_info_dual()

data_single = DataTreatment(variables, laser_list, 'Single')
data_single.valid_info_single()


        

        
        